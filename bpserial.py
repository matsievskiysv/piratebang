import serial
import yaml
import logging
import logging.config
from time import sleep
from typing import List, Union


class BPSerial():
    """BPSerial class for communicating with buspirate"""
    TIME_READ = 0.1  # time to wait for data in seconds

    def __init__(self) -> None:
        logging.config.dictConfig(yaml.load(open("logging.yaml", "r")))
        self.logger = logging.getLogger(__name__)
        self.__serial = serial.Serial()
        self.__serial.timeout = 100

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.disconnect()

    def configure(self,
                  port: Union[str, None] = None,
                  baud: int = None) -> None:
        """Configure serial port
        port: name of port. Defaults to None
        baud: communication baudrate. Defaults to 9600"""
        if port is not None:
            self.__serial.port = port
        if baud is not None:
            self.__serial.baud = baud

    def timeout(self, time: Union[int, None]) -> None:
        """Configure serial communication timeout.
        timeout: communication timeout.
See pyserial documentation for details"""
        self.__serial.timeout = time

    def connect(self) -> bool:
        """Connect to configured port.
        Output value: true for success, false for failure"""
        if not self.__serial.is_open:
            try:
                self.__serial.open()
                self.logger.info("""Serial open: opened port \
%s with baudrate %d""", self.__serial.port, self.__serial.baudrate)
                return True
            except serial.SerialException as e:
                self.logger.error("Serial open: %s", e)
                return False
        else:
            self.logger.warning("Serial port %s is already opened",
                                self.__serial.port)
            return False

    def disconnect(self) -> None:
        """Disconnect from port"""
        if self.__serial.is_open:
            self.__serial.close()
            self.logger.info("Serial port %s closed", self.__serial.port)
        else:
            self.logger.warning("Serial port %s is not opened",
                                self.__serial.port)

    def send_string(self, msg: str) -> None:
        """Write string to port
        str: data string"""
        if self.__serial.is_open:
            self.__serial.write(msg.encode())
            self.logger.debug("Serial write: %s", msg)
        else:
            self.logger.warning("Serial port %s is not opened",
                                self.__serial.port)

    def send_newline(self) -> None:
        """Send string newline to port"""
        if self.__serial.is_open:
            self.__serial.write(b'\n\r')
            self.__serial.flush()
            self.logger.debug("Serial write: \\n\\r")
        else:
            self.logger.warning("Serial port %s is not opened",
                                self.__serial.port)

    def send_bytes(self, msg: List[int]) -> None:
        """Send bytes to port
        msg: array of data to sent"""
        if self.__serial.is_open:
            self.__serial.write(bytes(msg))
            self.__serial.flush()
            self.logger.debug("Serial write %d bytes", len(msg))
        else:
            self.logger.warning("Serial port %s is not opened",
                                self.__serial.port)

    def send_confirm(self,
                     msg: List[int],
                     rv: List[bytes]) -> Union[List[bool], None]:
        """Send bytes to port and compare recieved value with expected
        msg: array of data to sent
        rv: array of expected return values
        Output value: array of bools, indicating match or miss"""
        if self.__serial.is_open:
            compared = []
            for i in range(0, len(msg)):
                self.__serial.write(bytes([msg[i]]))
                self.__serial.flush()
                ans = self.wait_recieve(self.TIME_READ)
                if ans is not None:
                    ansb: bool = ans == rv[i]
                    compared.append(ansb)
                else:
                    compared.append(False)
            return compared
        else:
            self.logger.warning("Serial port %s is not opened",
                                self.__serial.port)
            return None

    def recieve_bytes(self, n: int) -> Union[bytes, None]:
        """Get n bytes from port
        n: number of bytes to read
        Output value: string of read bytes"""
        if self.__serial.is_open:
            self.logger.debug("Serial read <= %d bytes", n)
            return self.__serial.read(n)
        else:
            self.logger.warning("Serial port %s is not opened",
                                self.__serial.port)
            return None

    def recieve_all(self) -> Union[bytes, None]:
        """Get all avaliable bytes from port
        Output value: string of read bytes"""
        if self.__serial.is_open:
            n = self.__serial.in_waiting
            if n > 0:
                self.logger.debug("Serial read %d bytes", n)
                return self.__serial.read(self.__serial.in_waiting)
            else:
                self.logger.debug("Serial read none avaliable")
                return None
        else:
            self.logger.warning("Serial port %s is not opened",
                                self.__serial.port)
            return None

    def wait_recieve(self, time: float) -> Union[bytes, None]:
        """Try to read all bytes from port for period of time (every t/10)
        time: time to wait for the data
        Output value: string of read bytes"""
        time_inc = time / 10
        if self.__serial.is_open:
            while True:
                n = self.__serial.in_waiting
                if n > 0:
                    return self.recieve_all()
                else:
                    if time <= 0:
                        self.logger.debug("Serial read none avaliable")
                        return None
                    else:
                        time = time - time_inc
                        sleep(time_inc)
        else:
            self.logger.warning("Serial port %s is not opened",
                                self.__serial.port)
            return None
