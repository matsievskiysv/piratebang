from bpbitbang import BPBB
import logging
import logging.config
from typing import Union, Optional, List

POWER = 0b_1000
PULLUP = 0b_0100
AUX = 0b_0010
CS = {"LOW": 0b0,
      "HIGH": 0b1}
SAMPLE_PHASE = {"MIDDLE": 0b0,
                "END": 0b1}
EDGE = {"I2A": 0b0,
        "A2I": 0b10}
IDLE = {"LOW": 0b0,
        "HIGH": 0b100}
OUT_TYPE = {"HIZ": 0b0,
            "3V3": 0b1000}
SPEED = {"30KHZ": 0b000,
         "125KHZ": 0b001,
         "250KHZ": 0b010,
         "1MHZ": 0b011,
         "2MHZ": 0b100,
         "2_6MHZ": 0b101,
         "4MHZ": 0b110,
         "8MHZ": 0b111}


class BPSPI(BPBB):
    """BPSPI class for bitbanging BusPiratel SPI interface"""

    def __init__(self) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

    def spi_version(self) -> Optional[str]:
        if self._mode == self._SPIMODE:
            ans = self._version_proto()
            if ans is None:
                self.logger.warning("Function unsuccessfull")
                return None
            else:
                return ans
        else:
            self.logger.warning("Not in SPI mode")
            return None

    def spi_per_config(self, mask: int) -> bool:
        if self._mode == self._SPIMODE:
            return self._per_config_proto(mask)
        else:
            self.logger.warning("Not in SPI mode")
            return False

    def spi_io_config(self, mask: int) -> bool:
        if self._mode == self._SPIMODE:
            return self._io_config_proto(mask)
        else:
            self.logger.warning("Not in SPI mode")
            return False

    def spi_speed(self, speed: Optional[int] = None) -> bool:
        ENABLE_READ = False
        if self._mode == self._SPIMODE:
            ans = self._speed_proto(speed)
            if not ENABLE_READ and speed is None:
                self.logger.warning("Not avaliable in this mode")
                return False
            if ans is None:
                self.logger.warning("Cannot set SPI speed")
                return False
            elif ans == self._MODE_CONFIRM:
                return True
            else:
                self.logger.warning("Cannot set SPI speed")
                return False
        else:
            self.logger.warning("Not in SPI mode")
            return False

    def spi_cs(self, value: bool) -> bool:
        if self._mode == self._SPIMODE:
            if value:
                # return self._cs_high_proto()
                return self._start_bit_proto()
            else:
                # return self._cs_low_proto()
                return self._stop_bit_proto()
        else:
            self.logger.warning("Not in SPI mode")
            return False

    def spi_tick(self) -> bool:
        # if self._mode == self._SPIMODE:
        #     return self._clock_tick_proto()
        # else:
        #     self.logger.warning("Not in SPI mode")
        #     return False
        self.logger.warning("Not avaliable in this mode")
        return False

    def spi_clock(self, value: bool) -> bool:
        # if self._mode == self._SPIMODE:
        #     if value:
        #         return self._clock_high_proto()
        #     else:
        #         return self._clock_low_proto()
        # else:
        #     self.logger.warning("Not in SPI mode")
        #     return False
        self.logger.warning("Not avaliable in this mode")
        return False

    def spi_data(self, value: bool) -> bool:
        # if self._mode == self._SPIMODE:
        #     if value:
        #         return self._data_high_proto()
        #     else:
        #         return self._data_low_proto()
        # else:
        #     self.logger.warning("Not in SPI mode")
        #     return False
        self.logger.warning("Not avaliable in this mode")
        return False

    def spi_peek(self) -> Optional[int]:
        # if self._mode == self._SPIMODE:
        #     ans = self._peek_pin_proto()
        #     if ans is None:
        #         return None
        #     else:
        #         return int.from_bytes(ans, byteorder="big")
        # else:
        #     self.logger.warning("Not in SPI mode")
        #     return None
        self.logger.warning("Not avaliable in this mode")
        return None

    def spi_bulk_clock(self, n: int) -> bool:
        self.logger.warning("Not avaliable in this mode")
        return False

    def spi_bulk_read(self, n: int) -> bool:
        self.logger.warning("Not avaliable in this mode")
        return False

    def spi_bulk_bits(self, n: int) -> bool:
        self.logger.warning("Not avaliable in this mode")
        return False

    def spi_bulk_bytes(self, msg: List[int]) -> List[Optional[int]]:
        if self._mode == self._SPIMODE:
            ans = self._bulk_bytes_proto(msg)
            if ans is None:
                return None
            else:
                lst: List[Optional[int]] = []
                for i in ans:
                    if i is not None:
                        for j in i:
                            lst.append(j)
                    else:
                        lst.append(None)
                return lst
        else:
            self.logger.warning("Not in SPI mode")
            return [None]
